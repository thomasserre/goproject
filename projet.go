package main
 
import (
	"fmt"
	"log"
	"net/http"
    "time"
	"database/sql"
	"encoding/json"
	"github.com/kare/base62"
	"github.com/gorilla/mux"
	_ "github.com/go-sql-driver/mysql"
)

//Structure BDD
type databaseinfos struct {
    user string
    password string
    name string
    address string
    port string
    url string
}

//Structure url dans BDD
type Url struct {
	Id 	int		`json:"id"`
	Url string	`json:"url"`
}

func Bddconnexion() string{
		//Connexion a la BDD
		dbi := databaseinfos{
			user: "root",
			password: "",
			name: "gobdd",
			address: "localhost",
			port: "3306",
			url: ""}
	
		dbi.url = (dbi.user + ":" + dbi.password + "@tcp(" + dbi.address + ":" + dbi.port + ")/" + dbi.name)

		return dbi.url
}

func NewURLHandler(w http.ResponseWriter, r *http.Request){
	var url Url
	if r.Method == "POST" {	
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&url)

		if err != nil {
			panic(err)
		}
		defer r.Body.Close()

		fmt.Printf("URL : %s\n", url.Url)
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(url)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed."))
	}

	db, err := sql.Open("mysql", Bddconnexion())
		if err != nil {
			panic(err.Error())
		}
		defer db.Close()

	//insertion d'url dans la BDD
	insert, err := db.Query("INSERT INTO urlshortener(url) VALUES('" + url.Url +"')")

	if err != nil {
        panic(err.Error())
	}
	defer insert.Close()

	//Recuperation de l'id
	results, err := db.Query("SELECT id FROM urlshortener order by id DESC LIMIT 1")
	if err != nil {
        panic(err.Error())
	}
	defer results.Close()

	for results.Next(){
		var test int64

		err = results.Scan(&test)
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("url de la bdd : %d\n", test)
		
		//Encodage de l'id
		encodeid := base62.Encode(test)
	
		//renvoye l'id encodé
		fmt.Printf("http://mapetiteurl.com/%s\n" , encodeid)
	}
	
}

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/api/v1/new", NewURLHandler).Methods("POST")
	
	srv := &http.Server{
        Handler:      r,
        Addr:         "localhost:8000",
        WriteTimeout: 30 * time.Second,
        ReadTimeout:  time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}